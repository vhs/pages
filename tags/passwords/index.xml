<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <docs>https://blogs.law.harvard.edu/tech/rss</docs>
    <title>passwords on VHSblog</title>
    <link>https://vhs.codeberg.page/tags/passwords/</link>
    <description>Recent content in passwords on VHSblog</description>
    <image>
      <title>passwords on VHSblog</title>
      <link>https://vhs.codeberg.page/tags/passwords/</link>
      <url>https://vhs.codeberg.page/images/8pd8ycjjkiq-maxime-le-conte-des-floris.jpg</url>
    </image>
    <ttl>1440</ttl>
    <generator>After Dark 10.0.0 (Hugo 0.101.0)</generator>
    <language>en-US</language>
    <managingEditor>0xc0000007b@tutanota.com (VHS)</managingEditor>
    <webMaster>0xc0000007b@tutanota.com (VHS)</webMaster>
    <copyright>Copyright &amp;copy; VHS. Licensed under CC-BY-ND-4.0.</copyright>
    <lastBuildDate>Mon, 20 Mar 2023 03:12:58 UT</lastBuildDate>
    <atom:link href="https://vhs.codeberg.page/tags/passwords/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Gopass and Git Password Management</title>
      <link>https://vhs.codeberg.page/post/gopass-git-password-management/</link>
      <pubDate>Sat, 06 Aug 2022 19:53:00 UT</pubDate>
      <dc:creator>VHS</dc:creator>
      <guid>https://vhs.codeberg.page/post/gopass-git-password-management/</guid>
      <description>After adopting a variation of the Gentoo policy for managing OpenPGP (GnuPG) keys I now feel confident enough to use OpenPGP to save sensitive passwords in the cloud. Gentoo&amp;rsquo;s GLEP 63 policy takes the guesswork out of key management and provides some best practices valuable when encrypting sensitive information.
Although saving sensitive passwords in the cloud may seem a foolish endeavor the alternative is to keep passwords on the sneakernet and risk losing them. Given the abundance of thumb drives and their general multi-purpose use one might, for example, accidentally erase their only backup. Not to mention saving data on any physical medium carries the risk the physical media becomes corrupted.
</description>
      <category domain="https://vhs.codeberg.page/categories/technology">Technology</category>
      <content:encoded><![CDATA[After adopting a variation of the Gentoo policy for managing OpenPGP (GnuPG) keys I now feel confident enough to use OpenPGP to save sensitive passwords in the cloud. Gentoo&amp;rsquo;s GLEP 63 policy takes the guesswork out of key management and provides some best practices valuable when encrypting sensitive information.
Although saving sensitive passwords in the cloud may seem a foolish endeavor the alternative is to keep passwords on the sneakernet and risk losing them. Given the abundance of thumb drives and their general multi-purpose use one might, for example, accidentally erase their only backup. Not to mention saving data on any physical medium carries the risk the physical media becomes corrupted.
With this in mind I personally have switched from my previous techniques to a terminal-based solution called gopass. Gopass is a drop-in replacement for Jason Donenfeld&amp;rsquo;s password store with an elegant, ncurses-style terminal-user interface. What&amp;rsquo;s more, gopass provides out of the box functionality to use Git to keep passwords synced between devices with a one-time per device set-up cost and driven entirely from the keyboard and ideal for copying an pasting passwords quickly and securely even while others are watching your screen.
If you&amp;rsquo;re not a terminal user or want to sync with your mobile device there are some fantastic GUIs for pass/gopass as noted on the pass website. I suspect as more users adopt this approach to password management the desire to place trust on third-parties such as 1Password will decrease, thereby improving Web security on the whole by reducing the number of honeypots.
How passwords are protected Using end-to-end encryption with data encrypted at rest. Thanks to GPG users may also leverage multi-factor authentication via password &#43; key. Users who set-up their OpenPGP keys using --expert mode can also leverage elliptic curve encryption via the ECC option for added security and durability during transit.
Where to store passwords online There are many free options for storing your passwords in Git. You could use a GitHub private repository, or even GitLab or a Gitea host such as codeberg.org.
Syncing with mobile As mentioned earlier there are a number of GUI clients for mobile. If you&amp;rsquo;re using Android, you can connect your Git repository to the Password Store app and both gopass and Password store will automatically update your connected Git repository anytime you make a change (manual syncs also possible). There are also some iOS apps for pass as mentioned on the pass site you could try out if you&amp;rsquo;re stuck running an Apple device.
Don&amp;rsquo;t want to store passwords online You don&amp;rsquo;t have to. If you&amp;rsquo;d prefer to keep all of your passwords offline just don&amp;rsquo;t set-up a Git remote. You will, however, need to find a way to sync them manually between your devices. And you&amp;rsquo;ll most certainly want to back them up using the 3-2-1 backup rule which could become arduous but some may prefer.
What about hardware wallets You can use those. Just be aware if they become lost so will your access to your passwords. If you rely on GPG you can set-up on on a few machines so losing one device (let alone a small USB dongle) won&amp;rsquo;t lock you out of your digital life.
Multi-factor authentication OTPs are one-time passwords used for two-factor authentication. If you&amp;rsquo;re a pass/gopass user you can keep OTPs stored alongside your passwords and eliminate the need for an additional applications such as Aegis Authenticator. Gopass has the ability to generate OTPs based on an otpauth URI which are easy to produce and modify using an offline-first security tool I built called OATHqr. But there are other ways as well so please use what works best for you.
]]></content:encoded>
    </item>
    <item>
      <title>Managing Passwords on Android</title>
      <link>https://vhs.codeberg.page/post/managing-passwords-on-android/</link>
      <pubDate>Tue, 29 Jan 2019 22:42:00 UT</pubDate>
      <dc:creator>VHS</dc:creator>
      <guid>https://vhs.codeberg.page/post/managing-passwords-on-android/</guid>
      <description>After hacking Android onto an HD2 previously running Windows Mobile I quickly became challenged with the task of recalling passwords for frequently used apps – apps like Telegram, ProtonMail, Binance, Snapchat you name it.
And although long-term password management may feel like a burdensome task to some a steadfast approach is critical for security and relatively painless for anyone who&amp;rsquo;s been using a KeePass port the last decade.
</description>
      <category domain="https://vhs.codeberg.page/categories/security">Security</category>
      <content:encoded><![CDATA[After hacking Android onto an HD2 previously running Windows Mobile I quickly became challenged with the task of recalling passwords for frequently used apps – apps like Telegram, ProtonMail, Binance, Snapchat you name it.
And although long-term password management may feel like a burdensome task to some a steadfast approach is critical for security and relatively painless for anyone who&amp;rsquo;s been using a KeePass port the last decade.
Update 2019-01-28: Over the last two years I&amp;rsquo;ve augmented my existing workflow with Bitwarden for 2FA-backed accounts, switched from Dropbox to MEGA and am experimenting with MacPass as my KeePass port for desktop.
Update 2016-08-10: I still use the same password management techniques described herein but have switched from KeePass to KeePassX on my desktop machine and began using MiniKeePass on my iOS devices.
For Android the KeePass port I&amp;rsquo;m using is KeePassDroid.
KeePassDroid makes recalling passwords as easy as copy/paste and also includes a password generator for creating strong passwords. keepassdroid.com KeePass ports like Droid store passwords in an encrypted file which may be shared between devices using a flash drive or over the Internet using encrypted cloud storage provider mega.nz from political activist Kim Dotcom.
To use KeePass with MEGA simply:
Get yourself a free MEGA account Install KeePass ports on your devices Use MEGAsync to sync the files With syncing enabled you will be able to modify your password database on one device and sync it with all your others using MEGA using end-to-end encryption and based on syncing rules you specify and control.
Promo: 50GB free cloud storage with MEGA when you sign-up for a new account. Simply email me and I&amp;rsquo;ll help you get set-up under my referral.
Not comfortable storing data in the cloud? You don&amp;rsquo;t have to. With KeyPass and its ports you can keep all your secrets on the Sneakernet if that&amp;rsquo;s your thing.
Speaking of Sneakernet KeePassX is also available on Tails OS. Tails is a privacy-focused &amp;ldquo;live OS&amp;rdquo; you can use to boot straight from USB into a Linux desktop on many systems – even on MacBooks.
And if you are a Mac user definitely check-out MacPass.
]]></content:encoded>
    </item>
    <item>
      <title>Password Protection with PassKeeper</title>
      <link>https://vhs.codeberg.page/post/password-protection-with-passkeeper/</link>
      <pubDate>Mon, 22 Nov 2010 00:00:00 UT</pubDate>
      <dc:creator>VHS</dc:creator>
      <guid>https://vhs.codeberg.page/post/password-protection-with-passkeeper/</guid>
      <description>After recently losing a USB flash drive with all my passwords on it, I was grateful for the precaution I took by storing my password data encrypted using Brad Greenlee’s PassKeeper password manager.
PassKeeper is a Windows utility that allows you to keep a list of accounts with usernames, passwords, and notes. This list is stored encrypted.
passkeeper.com </description>
      <category domain="https://vhs.codeberg.page/categories/security">Security</category>
      <content:encoded><![CDATA[After recently losing a USB flash drive with all my passwords on it, I was grateful for the precaution I took by storing my password data encrypted using Brad Greenlee’s PassKeeper password manager.
PassKeeper is a Windows utility that allows you to keep a list of accounts with usernames, passwords, and notes. This list is stored encrypted.
passkeeper.com Update 2016-11-22: Back in 2010 I switched to KeePass, which works on Android and provides features such as secure password generation and grouping. The utility is freeware and has been available for public download since the mid-90’s. Data are encrypted using the 56-bit DES cipher and stored in an DAT file in the application’s root directory. The size of the application (189 kilobytes) and the data file (~400 bytes/entry) are lightweight and can easily be carried around on any USB flash drive.
The application’s user interface (pictured left) is straight-forward and easy to use, and the system-oriented UI design has become more visually appealing as Windows has evolved.
One thing that hasn&amp;rsquo;t evolved, however, is the utility’s application icon (not pictured). The application icon has looked outdated since about Windows 98. But fixing the blemish is easy enough. Just create a Windows Shortcut and use a different icon. The imageres.dll located in %windir%\system32\ in Windows Vista contains a decent-looking padlock icon that can be used if desired.
With a little practice, the entire utility can be navigated using only the keyboard, and passwords can be quickly copied from PassKeeper and pasted into online forms and desktop applications without the use of a mouse. Coincidentally, the copy/paste behavior may help enhance security by masking password keystrokes from key loggers.
Over time, one noticeable drawback of using PassKeeper is that it does not provide a built-in password generator. Another is that passwords copied to the clipboard are not automatically cleared after a set amount of time, requiring the user to do so by some other means—if at all. There is also a bug with account names using certain special characters, though in my ten years using the utility I only saw it once. According to program readme.txt on passkeeper.com the utility is limited to 128 entries, but offers a simple workaround for the limitation.
Overall, PassKeeper is a straight-forward, easy-to-use utility for managing and securing personal passwords and account data. And though it&amp;rsquo;s starting to show its age, it continues run stably as Windows evolves. If you decide to use PassKeeper and carry around password data on a USB flash drive, the 56-bit encryption used should buy most users plenty of time to change any sensitive passwords should the device be lost.
Other password managers worth checking out KeePass Password Safe — A free open source password manager, which helps you to manage your passwords in a secure way. KeePassX &amp;ndash; Platform-independent port of KeePass Password Safe that works on Windows, Mac and Linux to name a few. Compatible with existing KeePass password databases. KeePassDroid &amp;ndash; A port of the KeePass Password Safe for the Android platform. Try it in conjunction with DropBox and KeePassX for a great cross-platform personal security solution. 1Password &amp;ndash; Widely-used, paid password management solution Password managers to pass up RoboForm — Though it has a version specifically for use with USB flash drives, RoboForm is reliant on a web browser to function; it is not suitable for managing desktop application passwords and may not function in all browsers.
]]></content:encoded>
    </item>
  </channel>
</rss>
