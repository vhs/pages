const fs = require('fs');
const util = require('util')
const parseString = require('xml2js').parseString;
const fileToParse = ('./index.xml')
const outputFilename=('output.json')

fs.readFile(fileToParse, function (err, data) {
    parseString(data, function (err, result) {
        console.dir(result);
        if (err) {
            console.log(err)
        }
       // do something with the JS object called result, or see below to save as JSON
        fs.writeFile('output.json', JSON.stringify(result), (writeErr) => {
            if (writeErr) throw writeErr;
            console.log('The file has been saved to ' + outputFilename);
        });
    });
});
